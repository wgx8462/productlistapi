package com.gb.productlistapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductItem {
    private Long id;
    private String name;
    private String imageName;
    private Double price;
}
