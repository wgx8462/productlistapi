package com.gb.productlistapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCreateRequest {
    private String name;
    private Double price;
    private String imageName;
}
