package com.gb.productlistapi.controller;

import com.gb.productlistapi.model.*;
import com.gb.productlistapi.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product")
public class ProductController {
    private final ProductService productService;

    @PostMapping("/join")
    public CommonResult setProduct(@RequestBody ProductCreateRequest request) {
        productService.setProduct(request);

        CommonResult response = new CommonResult();
        response.setMsg("등록 완료되었습니다.");
        response.setCode(0);

        return response;
    }

    @GetMapping("/all")
    public ListResult<ProductItem> getProducts() {
        List<ProductItem> list = productService.getProducts();

        ListResult<ProductItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }

    @GetMapping("/detail/{id}")
    public SingleResult<ProductResponse> getProduct(@PathVariable long id) {
        ProductResponse result = productService.getProduct(id);

        SingleResult<ProductResponse> response = new SingleResult<>();
        response.setMsg("성공하였습니다.");
        response.setCode(0);
        response.setData(result);

        return response;
    }

}
