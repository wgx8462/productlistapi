package com.gb.productlistapi.service;

import com.gb.productlistapi.entity.Product;
import com.gb.productlistapi.model.ProductCreateRequest;
import com.gb.productlistapi.model.ProductItem;
import com.gb.productlistapi.model.ProductResponse;
import com.gb.productlistapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public void setProduct(ProductCreateRequest request) {
        Product addData = new Product();
        addData.setName(request.getName());
        addData.setPrice(request.getPrice());
        addData.setImageName(request.getImageName());
        addData.setDateCreate(LocalDateTime.now());

        productRepository.save(addData);
    }

    public List<ProductItem> getProducts() {
        List<Product> originData = productRepository.findAll();
        List<ProductItem> result = new LinkedList<>();

        for (Product product : originData) {
            ProductItem addItem = new ProductItem();
            addItem.setId(product.getId());
            addItem.setName(product.getName());
            addItem.setImageName(product.getImageName());
            addItem.setPrice(product.getPrice());
            result.add(addItem);
        }
        return result;
    }

    public ProductResponse getProduct(long id) {
        // 창고지기한테 id 번의 데이터를 가져다 달라고 요청한다. --> 원본요청
        Product originData = productRepository.findById(id).orElseThrow();

        //새로 옮겨 담을 그릇을 준비한다.
        ProductResponse response = new ProductResponse();

        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPrice(originData.getPrice());
        response.setImageName(originData.getImageName());
        response.setDateCreate(originData.getDateCreate());

        return response; // 그릇에 다 옮겨담았으니 새로운 그릇을 돌려준다.
    }
}
